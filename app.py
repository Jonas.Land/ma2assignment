from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm, tryLogin, setUp
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
from flask_login import login_required


currentus = None

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = 'LKJasdja17HSDJ9898243ASDAJKLH8kajsdka/KJLKJ-lwea'

# Add a login manager to the app
setUp(app)

@app.route('/login', methods=['GET', 'POST'])
def cu():
    stillname = tryLogin()
    global currentus
    currentus = stillname[1]
    return stillname[0]

# This method is called whenever the login manager needs to get
# the User object for a given user id


# # This method is called to get a User object based on a request,
# # for example, if using an api key or authentication token rather
# # than getting the user name the standard way (from the session cookie)
# @login_manager.request_loader
# def request_loader(request):
#     # Even though this HTTP header is primarily used for *authentication*
#     # rather than *authorization*, it's still called "Authorization".
#     auth = request.headers.get('Authorization')

#     # If there is not Authorization header, do nothing, and the login
#     # manager will deal with it (i.e., by redirecting to a login page)
#     if not auth:
#         return

#     (auth_scheme, auth_params) = auth.split(maxsplit=1)
#     auth_scheme = auth_scheme.casefold()
#     if auth_scheme == 'basic':  # Basic auth has username:password in base64
#         (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
#         print(f'Basic auth: {uid}:{passwd}')
#         u = users.get(uid)
#         if u: # and check_password(u.password, passwd):
#             return user_loader(uid)
#     elif auth_scheme == 'bearer': # Bearer auth contains an access token;
#         # an 'access token' is a unique string that both identifies
#         # and authenticates a user, so no username is provided (unless
#         # you encode it in the token – see JWT (JSON Web Token), which
#         # encodes credentials and (possibly) authorization info)
#         print(f'Bearer auth: {auth_params}')
#         for uid in users:
#             if users[uid].get('token') == auth_params:
#                 return user_loader(uid)
#     # For other authentication schemes, see
#     # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

#     # If we failed to find a valid Authorized header or valid credentials, fail
#     # with "401 Unauthorized" and a list of valid authentication schemes
#     # (The presence of the Authorized header probably means we're talking to
#     # a program and not a user in a browser, so we should send a proper
#     # error message rather than redirect to the login page.)
#     # (If an authenticated user doesn't have authorization to view a page,
#     # Flask will send a "403 Forbidden" response, so think of
#     # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
#     abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    return tryLogin()

@app.get('/recive')
def recive():
    try:
        c = conn.execute('SELECT message FROM messages WHERE recipient GLOB ?;', [currentus])
        rows = c.fetchall()
        result = 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return f'ERROR: {e}'

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    stmt = f"SELECT * FROM messages WHERE message GLOB '{query}'"
    result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute('SELECT * FROM messages WHERE message GLOB ? ', (query))
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()   
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    try:
        reciver = request.args.get('sender') or request.form.get('sender')
        message = request.args.get('message') or request.args.get('message')
        if not reciver or not message:
            return f'ERROR: missing sender or message'
        conn.execute('INSERT INTO messages (sender, message, recipient) VALUES (?,?,?);', (currentus, message, reciver))
        return "Reciver: " + reciver + "\n" + message
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        message TEXT NOT NULL,
        recipient TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS userNamePassword (
        name TEXT PRIMARY KEY,
        password TEXT NOT NULL);''')
    c.execute('''INSERT OR REPLACE INTO  userNamePassword  (name, password)
    VALUES ("Alice", "trythis");''')
    
    
except Error as e:
    print(e)
    sys.exit(1)
