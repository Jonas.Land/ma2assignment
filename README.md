2a start and 2b

All the code for app is cramped into one file app.py. This includes login functions, send, search and announcement. This is problematic as it is hard to understand what the code does. Making it easier to make 
small mistakes which can lead to security risks. First thing to do is separate all the login functions from send, search and announcement, as this is a completely different part of the program.
After this is done, I can see that the SQL queries are susceptible to SQL injection as they do not use prepared statements. This needs to be fixed. Another problem is that the app secret key which enables encrypted
session data in cookies is set to 'mY s3kritz' which absolutely not a good key. This needs to be set as something random and unguessable.




Small summery of what I have done:
-Separated all login functions from app,
-Created a new table with names and passwords for users,
-encrypted the passwords before adding to the database in case someone got access to the table. I set this up an encounterd some porblems with type. I did not have time to fix this, but this is something that should and "is" implemented(commented out the hash code)
-Changed app secret key to something unguessable.
The application allows for a create user option and login. Then they can send and receive messages through the database.

Test:
open directory login-server and run command $ python -m flask run
type inn username and password press createuser.
then log in with the newly created username and password.
here you can send messages to another existing user, and they can open this message by pressing view all messages.

who might attack the application?
- people with malicious intent, for example get access to passwords
- bored teens
- people looking for some entertainment, and wants to see if they can do it

An attacker can in theory get access to the database which includes passwords. and leak these, this problem is a bit smaller when all passwords are encrypted with a one-way encryption such as sha256 where it is in theory 
impossible to reconstruct the original string. The limits to what an attacker can do in this case comes from the fact that very little personal information is stored. The only real person information is the password and messages. 
So beyond this there is not really much an attacker can get from this program.

Are there limits to what we can sensibly protect against?
In theory everything "should" be protected against, but this is extremally difficult as attacks are evolving just as fast as security measures.
For this reason, I think that for this program which does not contain any super personal information besides password and maybe messages, it is most important to focus on the most common attack methods.

What are the main attack vectors for the application?
I would say the main attack vectors for this program is cross site request attack and SQL injections. The reason for this is that there is a lot of SQL injection code which an attacker might want to try to exploit.
Cross site request attack as the current user detection is not so good so an attacker might be able to act as someone else and get access to their messages.

What should we do (or what have you done) to protect against attacks?
The code should be tested thoroughly and at least be protected against the most common attacks such as cross site scripting(xss), SQL injection and cross site request attack and other common attacks. I have protected the code 
against SQL injection, but I would not be confident if this application were to be released at this time. I have simply not been able to spend as much time I would wish to reorganize the code and test it against attacks such as 
xss attacks and other common attack methods.

What is the access control model?
Access control model is a system that determines what a user is allowed to access. For example, even though a user is logged in this does not mean the user can do whatever they want. There are usually limits to protect them and 
other users. There might be admin users who will have more access than just a normal user, and people who are not logged in will also have some accessibility to for example the login screen.



How can you know that your security is good enough?
It is extremely difficult to know exactly when the program is secure enough. What is important is to not rush the release of the software before the security is thoroughly tested. And be ready to patch mistakes fast if/when they come, 
this is all that can be done as both attacks and security is constantly evolving. In other words, no website is ever truly secure.



