from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
import apsw
import flask
from apsw import Error
from flask import request, render_template
import flask_login
from flask_login import login_user


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    createuser = SubmitField('createuser')
    submit = SubmitField('submit')



login_manager = flask_login.LoginManager()
def setUp(app):
    login_manager.init_app(app)
    login_manager.login_view = "login"
    
conn = apsw.Connection('./tiny.db')

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(user_id):
    # For a real app, we would load the User from a database or something
    user = User()
    user.id = conn.execute("SELECT name FROM userNamePassword WHERE name GLOB ?", [user_id])
    return user


def createuser(name, passw):
    c = conn.cursor()
    form = LoginForm()
    try:
        c.execute(f'''INSERT INTO userNamePassword VALUES (?, ?);''', (name,passw))
        return render_template('./login.html', form=form)
    except:
        print("username already in use try anotherone")
        return render_template('./login.html', form=form)
       






def tryLogin():
    c = conn.cursor()
    form = LoginForm()
    passw = form.password.data
    username = form.username.data
    #hashpass = hashlib.sha256(passw.encode('utf-8')).hexdigest()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
        
    if form.validate_on_submit():
        if form.createuser.data:
            createuser(username ,passw)

        if form.submit.data:
            userpass = [(username, passw)]
            u = c.execute('SELECT name, password FROM userNamePassword WHERE name GLOB ?', [username])
            datauserpass = u.fetchall()
            if datauserpass == userpass: 
                user = user_loader(username)
                
                # automatically sets logged in session cookie
                login_user(user)

                flask.flash('Logged in successfully.')

                next = flask.request.args.get('next')
        
                # is_safe_url should check if the url is safe for redirects.
                # See http://flask.pocoo.org/snippets/62/ for an example.
                if False and not is_safe_url(next):
                    return flask.abort(400)

                return (flask.redirect(next or flask.url_for('index')), username)
    return (render_template('./login.html', form=form), None)